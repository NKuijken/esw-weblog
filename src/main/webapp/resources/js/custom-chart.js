/* Custom functions that help in getting remote data and drawing a chart to a div */

function createNewLineChart(divId) {
    var chart = {
        options: {
            chart: {
                renderTo: divId,
                backgroundColor: '#444c57'
            }
        }
    };
    chart = jQuery.extend(true, {}, getBaseChart(), chart);
    chart.init(chart.options);
    return chart;
}


function getBaseChart() {

    var baseChart = {
        highchart: null,
        defaults: {
            credits: {
                enabled: false
            },
            exporting: {
                enabled: true
            },
            title: {
                text: null,
                x: -20,
                style: {
                    color: '#989898',
                    fontWeight: 'bold',
                    fontSize: '16px',
                    fontFamily: 'Ruda, sans-serif'
                }
            },
            xAxis: {
                categories: [],
                gridLineDashStyle: 'dot',
                gridLineColor: '#989898',
                gridLineWidth: 1,
                tickColor: '#989898',
                tickWidth: 2,
                title: {
                    text: null,
                    style: {
                        color: '#989898',
                        fontWeight: 'bold',
                        fontSize: '12px',
                        fontFamily: 'Ruda, sans-serif'
                    }
                },
                labels: {
                    rotation: -25,
                    align: 'right',
                    style: {
                        color: '#989898',
                        fontWeight: 'normal',
                        fontSize: '9px',
                        fontFamily: 'Ruda, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                gridLineWidth: 1,
                gridLineColor: '#989898',
                gridLineDashStyle: 'solid',
                title: {
                    text: null,
                    style: {
                        color: '#989898',
                        fontWeight: 'bold',
                        fontSize: '12px',
                        fontFamily: 'Ruda, sans-serif'
                    }
                },
                labels: {
                    style: {
                        color: '#989898',
                        fontSize: '12px',
                        fontFamily: 'Ruda, sans-serif'
                    }
                },
                plotLines: [{
                    value: 0,
                    width: 1
                }]
            },
            tooltip: {
                crosshairs: true,
                formatter: function() {
                    return '<b>'+ this.series.name +'</b><br/>'+
                        this.x +': '+ this.y;
                }
            },
            legend: {
                layout: 'horizontal',
                backgroundColor: '#444c57',
                align: 'center',
                verticalAlign: 'top',
                borderWidth: 0,
                shadow: false,
                style: {
                    color: '#989898',
                    fontWeight: 'bold',
                    fontSize: '9px',
                    fontFamily: 'Ruda, sans-serif'
                }
            },
            series: []

        },

        // here you'll merge the defaults with the object options
        init: function(options) {
            this.highchart = jQuery.extend({}, this.defaults, options);
        },

        create: function() {
            new Highcharts.Chart(this.highchart);
        }

    };
    return baseChart;
}//function end


function getRemoteDataDrawChart(url, linechart) {

    $.ajax({
        url: url,
        dataType: 'json',
        success: function(data) {

            var categories = data.categories;
            var title = data.title;
            var yTitle = data.yAxisTitle;
            var xTitle = data.xAxisTitle;
            var divId =  data.divId;

            //populate the lineChart options (highchart)
            linechart.highchart.xAxis.categories = categories;
            linechart.highchart.title.text = title;
            linechart.highchart.yAxis.title.text = yTitle;
            linechart.highchart.xAxis.title.text = xTitle;
            linechart.highchart.chart.renderTo = divId;

            $.each(data.series, function(i, seriesItem) {
                console.log(seriesItem) ;
                var series = {
                    data: []
                };
                series.name = seriesItem.name;
                series.color = seriesItem.color;

                $.each(seriesItem.data, function(j, seriesItemData) {
                    console.log("Data (" + j +"): "+seriesItemData) ;
                    series.data.push(parseFloat(seriesItemData));
                });

                linechart.highchart.series[i] = series;
            });

            //draw the chart
            linechart.create();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            $('#'+linechart.options.chart.renderTo).html('<p style="color: #ed5565;">Error occurred building graph: '+thrownError+'</p>');
        },
        cache: false
    });
} //function end