/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CITest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Niek Kuijken
 */
public class CITestTest {
    
    public CITestTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testSomeMethod() {
        
    }


    /**
     * Test of GoodTest method, of class CITest.
     */
    @Test
    public void testGoodTest() {
        System.out.println("GoodTest");
        int x = 4;
        int y = 6;
        CITest instance = new CITest();
        int expResult = 10;
        int result = instance.GoodTest(x, y);
        assertEquals(expResult, result);
        
        System.out.println("GoodTest results: " + x + " + " + y + " = " + result);
    }

    /**
     * Test of BadTest method, of class CITest.
     */
    @Test
    public void testBadTest() {
        System.out.println("BadTest");
        int x = 5;
        int y = 10;
        CITest instance = new CITest();
        int expResult = 6;
        int result = instance.BadTest(x, y);
        assertEquals(expResult, result);
    }
    
}
