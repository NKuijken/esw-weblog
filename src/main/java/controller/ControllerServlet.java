/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import static java.net.Proxy.Type.HTTP;
import java.util.Iterator;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Comment;
import model.Posting;
import service.WebLogService;


import com.google.gson.Gson;

/**
 *
 * @author Niek Kuijken
 */
@WebServlet(name = "ControllerServlet", urlPatterns = {"/ControllerServlet", "/WeblogAdm", "/Weblog"})
public class ControllerServlet extends HttpServlet  {

    // create weblogservice on init ControllerServlet class (to remember Postings/comments during 1 session)
    WebLogService wls = new WebLogService();
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * ONLY USED FOR PRINTLINE HTML -> IN OTHER WORDS, NEVER USE THIS
     * 
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {

            WebLogService weblogService = new WebLogService();
            List<Posting> postings = weblogService.getPostings();

            out.println("<head>");
            out.println("<title>Servlet WeblogServlet serveert postings vanaf heden:</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Alle postings:</h1>");

            for (Posting p : postings) {
                out.println("<br> titleTest: " + p.getTitle());
            }

            out.println("</body>");
            out.println("</html>");

        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
     
        String userPath = request.getServletPath();
        if (userPath.equals("/Weblog")) {
                       
            // forward to page with all Postings + comments..
            List<Posting> allPostings = wls.getPostings();
            request.setAttribute("allPostings", allPostings);
            
            RequestDispatcher rd = request.getRequestDispatcher("views/Weblog.jsp");
            
            rd.forward(request, response);
        } else {
            if (userPath.equals("/WeblogAdm")) {
                // forward to page for new Posting..
                RequestDispatcher rd = request.getRequestDispatcher("views/WeblogAdm.jsp");
                rd.forward(request, response);
            } else {

                RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                rd.forward(request, response);
            }
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String userPath = request.getServletPath();
        System.out.println("Userpath :" + userPath);
        
        // POST request for Weblog -> add comment
        if (userPath.equals("/Weblog")) {
              
            
            /**
             * Added JSON handling below 
             * 
             * Note: Couldn't get JAVA to parse the JSON send by /Weblog..
             */
            
//            response.setContentType("application/json");       
//            Gson gson = new Gson();
//            
//            try {
//                StringBuilder sb = new StringBuilder();
//                String s;
//                while ((s = request.getReader().readLine()) != null) {
//                    sb.append(s);
//                }
//
//                Comment comment = (Comment) gson.fromJson(sb.toString(), Comment.class);
//
//                response.getOutputStream().print(gson.toJson(comment));
//                response.getOutputStream().flush();
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                response.getOutputStream().print(gson.toJson("Fail"));
//                response.getOutputStream().flush();
//            }
            
            /**
             * End of JSON handling
             */
            
            /**
             * Conventional handling via POST
             */
            
            Long postId = Long.parseLong(request.getParameter("postId"));
            String comment = request.getParameter("comment");
            
            Comment c = new Comment(postId, comment);
                        
            List<Posting> allPostings = wls.getPostings();
            
            for(Posting p : allPostings){
                // find Posting which the comment is meant for
                if(p.getId().equals(postId)){
                    // get List of existing comments
                    List<Comment> allComments = p.getComments();
                    // add new comment to the list
                    allComments.add(c);
                }
            }

            // set the updated Posting list as an attribute and return to the view
            request.setAttribute("allPostings", allPostings);
            RequestDispatcher rd = request.getRequestDispatcher("views/Weblog.jsp");
            rd.forward(request, response);
            
            /**
             * End of Conventional handling via POST (forwarding to updated view)
             */
            
        } else {     
            // POST request for WeblogAdm -> add Posting
            if (userPath.equals("/WeblogAdm")) {
                
                // get form-fields on WeblogAdm page
                String title = request.getParameter("title");
                String content = request.getParameter("content");
                
                // create new post with parameters
                Posting newPost = new Posting(title, content);
                
                // add to list through weblog-service
                wls.addPosting(newPost);                
                
                // get all postings and return page
                List<Posting> allPostings = wls.getPostings();

                request.setAttribute("allPostings", allPostings);
                RequestDispatcher rd = request.getRequestDispatcher("views/Weblog.jsp");
                rd.forward(request, response);
//                response.sendRedirect("Weblog");

            } else {
                RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
                rd.forward(request, response);
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
