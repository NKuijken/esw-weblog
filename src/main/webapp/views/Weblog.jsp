<%-- 
    Document   : Weblog
    Created on : Aug 29, 2017, 8:15:01 PM
    Author     : Niek Kuijken
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ATeam">
        <meta name="keyword" content="">

        <!-- DEFAULT -->
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/lineicons/style.css" />" rel="stylesheet">
        
        <!-- ATeam Styles -->
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">

        <!-- SCRIPTS -->
        <script src="<c:url value="/resources/js/jquery.js" />"></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.ui.touch-punch.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.iframe-transport.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.ui.widget.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.fileupload.js" />"></script>
        <script class="include" type="text/javascript" src="<c:url value="/resources/js/jquery.dcjqaccordion.2.7.js" />"></script>
        
        <title>Weblog</title>
    </head>
    <body>
        
        <script>        
            // Couldn't get the servlet to parse the JSON, which is build by below function..
            
            
            $(function () {

                $('form').on('submit', function(e) {
                                    
                    var id = $(this).data('id');
                    var postId = $('#postId' + id).val();
                    var comment = $('#comment' + id).val();
                                      
                    dataType: 'json',
                            e.preventDefault();
                    e.stopPropagation();
                    
                    var formdata = {
                        "postId": postId,
                        "comment": comment
                    };
                  
                    alert(JSON.stringify(formdata));

                    $.ajax({
                        type: "POST",
                        url: "/Weblog",
                        data: JSON.stringify(formdata),
                        contentType: 'application/json',
                        success: function (data) {
                            if (data !== "Fail")
                                alert('Added comment!');
                            else
                                alert('Failed adding comment');
                        }
                    });
                    
                })
            });
        </script>
        
        
        <div class="container">
            <h1 text-align="center">Weblog deluxe</h1>

            <a href ="WeblogAdm">WeblogAdm: Klik hier om een Posting toe te voegen</a>
        </div>

        <c:forEach items="${allPostings}" var="p">

            <div id="div id" class="container">
                <h2><c:out value="${p.getTitle()}" /> (ID: <c:out value="${p.getId()}" />)</h2>
                <p>Date: <c:out value="${p.getDate()}" /></p>            
                <p><c:out value="${p.getContent()}" /></p>

            <br />  

            <div class="container-fluid" style="border:1px solid #cecece;">
                <br/>

                <b>Comments..</b>
                <c:forEach items="${p.getComments()}" var="c">
                    <p><font color="red"><c:out value="${c.getContent()}" /> (<c:out value="${c.getDate()}" />)</font></p>
                </c:forEach>

                <br/><br/>

                <form action="Weblog" method="POST">
                    Add comment:<br>
                    <input id="postId" name="postId" type="hidden" value="${p.getId()}">
                    <input type="textarea" id="comment" name="comment" placeholder="Mockup comment..">
                    
                    <br><br>
                    
                    <input type="submit" value="Submit">
                </form> 
            </div>   

            <br /><hr /><br /> 
            </div>
        </c:forEach>
                
    </body>
</html>
