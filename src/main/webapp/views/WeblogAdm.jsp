<%-- 
    Document   : WeblogAdm
    Created on : Aug 29, 2017, 8:14:34 PM
    Author     : Niek Kuijken
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="ATeam">
        <meta name="keyword" content="">

        <!-- DEFAULT -->
        <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
        <link href="<c:url value="/resources/lineicons/style.css" />" rel="stylesheet">
        
        <!-- ATeam Styles -->
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">

        <!-- SCRIPTS -->
        <script src="<c:url value="/resources/js/jquery.js" />"></script>
        <script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.ui.touch-punch.min.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.iframe-transport.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.ui.widget.js" />"></script>
        <script src="<c:url value="/resources/js/jquery.fileupload.js" />"></script>
        <script class="include" type="text/javascript" src="<c:url value="/resources/js/jquery.dcjqaccordion.2.7.js" />"></script>
        
        <title>WeblogAdm</title>
    </head>
    <body>
        
        <div class="row mt">
            <div class="col-md-12 col-sm-offset-1">
        
                <h1>New post:</h1>
                <!--simple form for adding a Posting-->
                <form action="WeblogAdm" method="POST">
                    Title:<br>
                    <input type="text" name="title" placeholder="Mockup title..">
                    <br><br>
                    Content:<br>
                    <textarea name="content" rows="4" cols="50" placeholder="Mockup content..">

                    </textarea>
                    <!--<input type="textarea" name="content" placeholder="Mockup content..">-->
                    <br><br>
                    <input type="submit" value="Submit">
                </form> 
            </div>
        </div>
    </body>
</html>
